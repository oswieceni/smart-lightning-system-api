﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.ProfileProperties;
using static Nancy.HttpStatusCode;

namespace SmartLightningSystemApi.Modules
{
  public class FlowModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public FlowModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/Flow")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<FlowProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("flo", properties.Id, (int)properties.speed);
        if (!sentOk) return Response.AsJson(ErrorMessage(Context.Request.Body.ToString()), NotAcceptable);

        _settingsService.UpdateDeviceSettings(properties.Id, properties);
        return Response.AsJson($"Changed successfully to {Enum.GetName(typeof(Speed), properties.speed)}");
      });
    }
  }
}