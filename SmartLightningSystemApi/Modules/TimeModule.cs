﻿using System;
using System.Net.Http;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.ProfileProperties;
using static Nancy.HttpStatusCode;

namespace SmartLightningSystemApi.Modules
{
  public class TimeModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public TimeModule(ISettingsService settingsService,
      IEmbeddedConnectionService embeddedConnectionService) : base("/Time")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<TimeProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("tim", properties.Id, properties.StartHour,
          properties.StartMinute, properties.EndHour, properties.EndMinute);
        if (!sentOk) return Response.AsJson(ErrorMessage(Request.Body.ToString()), NotAcceptable);

        _settingsService.UpdateDeviceSettings(properties.Id, properties);
        return Response.AsJson("Time profile settings has been changed");
      });
    }
  }
}
