﻿using System;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.ProfileProperties;
using static Nancy.HttpStatusCode;

namespace SmartLightningSystemApi.Modules
{
  public class ActivityModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public ActivityModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/activity")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<ActivityProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("acv", properties.Id, (int) properties.Activity);
        if (!sentOk)
          return Response.AsJson(ErrorMessage(Request.Body.ToString()), NotAcceptable);

        _settingsService.UpdateDeviceSettings(properties.Id, properties);
        return Response.AsJson(
          $"Changed successfully (Activity: {Enum.GetName(typeof(Activity), properties.Activity)})");
      });
    }
  }
}