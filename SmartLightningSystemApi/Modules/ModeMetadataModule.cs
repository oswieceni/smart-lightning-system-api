﻿using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Nancy.Swagger.Modules;
using Nancy.Swagger.Services;
using Nancy.Swagger.Services.RouteUtils;
using Swagger.ObjectModel;

namespace SmartLightningSystemApi.Modules
{
    public class ModeMetadataModule : SwaggerMetadataModule
    {
        public ModeMetadataModule(ISwaggerModelCatalog modelCatalog, ISwaggerTagCatalog tagCatalog) : base(modelCatalog, tagCatalog)
        {
            RouteDescriber.DescribeRouteWithParams<ModeModule>("ChangeMode", "", "Change Mode",
                new HttpResponseMetadata[]{}, new[]
                {
                    new Parameter
                    {
                        Name = "Mode",
                        In = ParameterIn.Body,
                        Required = true,
                        Description = "Profil, który powinien zostać aktywowany po wywołaniu akcji",
                        Type = "int"
                    }
                });
        }
    }
}