﻿using Nancy;

namespace SmartLightningSystemApi.Modules
{
  public abstract class BaseModule : NancyModule
  {
    protected BaseModule(string modulePath) : base(modulePath)
    {
    }

    protected BaseModule()
    {
    }

    protected static string ErrorMessage(string body) => $"Invalid parameter value: {body}";
  }
}