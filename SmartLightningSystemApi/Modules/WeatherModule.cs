﻿using Microsoft.Extensions.Logging;
using Nancy;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Database;

namespace SmartLightningSystemApi.Modules
{
  public class WeatherModule : BaseModule
  {
    private readonly IWeatherService _weatherService;
    private const int Red = 0xee1010;
    private const int Green = 0x37c759;
    private const int Blue = 0x044bb5;
    private ILogger<WeatherModule> _logger;

    public WeatherModule(IWeatherService weatherService, LoggerFactory loggerFactory) : base("/weather")
    {
      _weatherService = weatherService;
      _logger = loggerFactory.CreateLogger<WeatherModule>();
      
      Get("/", _ => Response.AsJson(_weatherService.GetWeather()));

      Get("/color", _ =>
      {
        var weather = _weatherService.GetWeather();
        var result = $"color:{ObtainColor(weather)}";
        _logger.LogInformation(result);
        return Response.AsText(result);
      });

      Get("/complete", _ =>
      {
        var weather = new WeatherComplete(_weatherService.GetWeather());
        weather.Color = $"#{ObtainColor(weather)}";
        return Response.AsJson(weather);
      });
    }

    private static string ObtainColor(Weather weather)
    {
      int color;
      if (weather.CloudCoverage < 33) color = Red;
      else if (weather.CloudCoverage < 66) color = Green;
      else color = Blue;
      return $"{color:x6}";
    }
  }
}
