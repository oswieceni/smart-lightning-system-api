﻿using System;
using System.Linq;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Database;
using SmartLightningSystemApi.Models;

namespace SmartLightningSystemApi.Modules
{
  public class DevicesModule : BaseModule
  {
    private readonly ISettingsService _settingsService;

    public DevicesModule(ISettingsService settingsService) : base("/Devices")
    {
      _settingsService = settingsService;

      Get("/", _ => _settingsService.GetDevices().Select(device => new
      {
        device.DeviceId,
        device.DeviceName,
        device.Type,
        device.WorkMode
      }));

      Get("/{Id:int}", parameters =>
      {
        try
        {
          return _settingsService.GetDeviceSettings(parameters.Id);
        }
        catch (Exception e)
        {
          return new Device();
        }
      });

      Get("/{Id:int}/{Mode:int}",
        parameters => (_settingsService.GetDeviceSettings(parameters.Id) as Device).Settings[(Modes) parameters.Mode]);

      Post("/", _ =>
      {
        var properties = this.Bind<DeviceName>();
        foreach (var device1 in Database.Database.Devices.Where(device => device.Key == properties.Id))
        {
          device1.Value.DeviceName = properties.Name;
        }

        return true;
      });
    }

    internal class DeviceName
    {
      public int Id { get; set; }
      public string Name { get; set; }
    }
  }
}
