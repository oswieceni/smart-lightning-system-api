﻿using Nancy;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Modules;

namespace SmartLightningSystemApi.Module
{
  public class SettingsModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public SettingsModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/Settings")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      //Get("/", _ => );
    }
  }


}
