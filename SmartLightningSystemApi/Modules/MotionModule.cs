﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.ProfileProperties;

namespace SmartLightningSystemApi.Modules
{
  public class MotionModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public MotionModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/Motion")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<MotionProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("mov", properties.Id, properties.Delay);
        if (!sentOk)
          return Response.AsJson(ErrorMessage(Context.Request.Body.ToString()), HttpStatusCode.NotAcceptable);

        _settingsService.UpdateDeviceSettings(properties.Id, properties);
        return Response.AsJson("OK");
      });
    }
  }
}
