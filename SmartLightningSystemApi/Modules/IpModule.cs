using System.Linq;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;

namespace SmartLightningSystemApi.Modules
{
  public class IpModule : NancyModule
  {
    private ISettingsService _settingsService;
    private ILogger<IpModule> _logger;
    
    public IpModule(ISettingsService settingsService, ILoggerFactory loggerFactory) : base("/Ip")
    {
      _settingsService = settingsService;
      _logger = loggerFactory.CreateLogger<IpModule>();
      
      Post("/acu", _ =>
      {
        _logger.LogInformation(this.Context.Request.Body.ToString());
        
        var settings = this.Bind<ArduinoIpSettingsModel>();
        
        _settingsService.ChangeAcuSettings(settings);

        return Response.AsJson(_settingsService.GetAcuSettings());
      });
    }
  }

  public class ArduinoIpSettingsModel
  {
    public MethodEnum Method { get; set; }
    public string Ip { get; set; }
    public int Id { get; set; }
  }

  public enum MethodEnum
  {
    LocalNetwork,
    GlobalNetwork
  }
}
