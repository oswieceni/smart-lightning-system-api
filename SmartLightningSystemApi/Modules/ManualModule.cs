﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.ProfileProperties;

namespace SmartLightningSystemApi.Modules
{
  public class ManualModule : BaseModule
  {
    private readonly ISettingsService _settingsService;
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public ManualModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/Manual")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<ManualProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("man", properties.Id, (int) properties.State,
          properties.Color.ToString("x6"), properties.Brightness);
        if (!sentOk)
          return Response.AsJson(ErrorMessage(Context.Request.Body.ToString()), HttpStatusCode.NotAcceptable);

        _settingsService.UpdateDeviceSettings(properties.Id, properties);
        return Response.AsJson(
          $"Changed successfully (State: {Enum.GetName(typeof(State), properties.State)}, " +
          $"Color: #{properties.Color:x6}, Brightness: {properties.Brightness})");
      });
    }
  }
}
