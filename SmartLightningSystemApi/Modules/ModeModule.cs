﻿using System;
using System.Net.Http;
using Nancy;
using Nancy.ModelBinding;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using static Nancy.HttpStatusCode;

namespace SmartLightningSystemApi.Modules
{
  public class ModeModule : BaseModule
  {
    private ISettingsService _settingsService;
    private IEmbeddedConnectionService _embeddedConnectionService;

    public ModeModule(ISettingsService settingsService, IEmbeddedConnectionService embeddedConnectionService) : base("/Mode")
    {
      _settingsService = settingsService;
      _embeddedConnectionService = embeddedConnectionService;

      Post("/", _ =>
      {
        var properties = this.Bind<ModeProfileProperties>();

        var sentOk = _embeddedConnectionService.SendCommand("mod", properties.Id, (int) properties.Mode);
        if (!sentOk)
          return Response.AsJson(ErrorMessage(Context.Request.Body.ToString()), NotAcceptable);

        _settingsService.SetWorkMode(properties.Id, properties.Mode);
        return Response.AsJson($"Changed successfully to {Enum.GetName(typeof(Modes), properties.Mode)}");
      });
    }
  }
}
