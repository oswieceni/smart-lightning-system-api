﻿using System.Collections.Generic;
using SmartLightningSystemApi.Models;

namespace SmartLightningSystemApi.Database
{
  public class Device
  {
    public int DeviceId { get; set; }
    public Modes WorkMode { get; set; }
    public DeviceType Type { get; set; }
    public string DeviceName { get; set; }
    public Dictionary<Modes, ProfileProperties.ProfileProperties> Settings { get; set; }
  }

  public enum DeviceType
  {
    KobiIr, LedTape
  }
}