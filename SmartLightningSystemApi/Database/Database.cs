﻿using System;
using System.Collections.Generic;
using SmartLightningSystemApi.Modules;

namespace SmartLightningSystemApi.Database
{
  public static class Database
  {
    public static string ArduinoIp { get; set; } = "http://192.168.0.130";

    private static Weather _weather;
    private static Dictionary<int, Device> _devices = new Dictionary<int, Device>();

    public static Dictionary<int, Device> Devices
    {
      get => _devices ?? (_devices = new Dictionary<int, Device>());
      set => _devices = value;
    }

    public static Weather Weather
    {
      get => _weather ?? (_weather = new Weather());
      set => _weather = value;
    }

    public static MethodEnum Method { get; set; }
    public static int ArduinoId { get; set; }
  }

  public class Weather
  {
    public Weather()
    {
      LastUpdated = DateTime.MinValue;
    }

    public Weather(Weather weather)
    {
      LastUpdated = weather.LastUpdated;
      Location = weather.Location;
      Temperature = weather.Temperature;
      TemperatureFeeled = weather.TemperatureFeeled;
      IsDay = weather.IsDay;
      Condition = weather.Condition;
      CloudCoverage = weather.CloudCoverage;
      Source = weather.Source;
    }

    public DateTime LastUpdated { get; set; }
    public string Location { get; set; }
    public decimal Temperature { get; set; }
    public bool IsDay { get; set; }
    public Condition Condition { get; set; }
    public int CloudCoverage { get; set; }
    public decimal TemperatureFeeled { get; set; }
    public string Source { get; set; }
  }

  public class Condition
  {
    public string Text { get; set; }
    public string Icon { get; set; }
  }

  public class WeatherComplete : Weather
  {
    public WeatherComplete(Weather weather) : base(weather)
    {
    }

    public string Color { get; set; }
  }
}
