using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.TinyIoc;
using SmartLightningSystemApi.BusinessLogic.Services;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using System;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.Conventions;

namespace SmartLightningSystemApi
{
  public class Bootstrapper : DefaultNancyBootstrapper
  {
    private readonly IAppSettings appSettings;
    private readonly IServiceProvider serviceProvider;

    public Bootstrapper()
    {
    }

    public Bootstrapper(IAppSettings appSet, IServiceProvider serviceProv)
    {
      appSettings = appSet;
      serviceProvider = serviceProv;
    }

    protected override void ConfigureApplicationContainer(TinyIoCContainer container)
    {
      container.Register(appSettings);
      container.Register(serviceProvider.GetService<ILoggerFactory>());

      container.Register<IVersionService, VersionService>();
      container.Register<IWeatherService, ApixuWeatherService>();
      container.Register<IEmbeddedConnectionService, Esp8266ConnectionService>();
      container.Register<ISettingsService, SettingsService>();
    }

    public override void Configure(INancyEnvironment environment)
    {
      environment.Tracing(enabled: false, displayErrorTraces: true);
    }

    protected override void ConfigureConventions(NancyConventions nancyConventions)
    {
      base.ConfigureConventions(nancyConventions);

      nancyConventions.StaticContentsConventions.Add(
        StaticContentConventionBuilder.AddDirectory("/swagger-ui/dist"));
    }

    protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
    {
      pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
      {
        ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
          .WithHeader("Access-Control-Allow-Methods", "POST,GET")
          .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
      });
    }
  }
}
