﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using static System.String;

namespace SmartLightningSystemApi.BusinessLogic.Services
{
  public class Esp8266ConnectionService : IEmbeddedConnectionService
  {
    private readonly HttpClient _httpClient;
    private readonly ILogger<Esp8266ConnectionService> _logger;

    public Esp8266ConnectionService(ILoggerFactory loggerFactory)
    {
      _logger = loggerFactory.CreateLogger<Esp8266ConnectionService>();
      _httpClient = new HttpClient();
    }

    public bool SendCommand(string command, params object[] args)
    {
      try
      {
        var url = PrepareUrlForCommand(command, args);
        _logger.LogInformation(url.ToString());
        var task = _httpClient.GetAsync(url);
        return task.Result.IsSuccessStatusCode;
      }
      catch (Exception e)
      {
        _logger.LogCritical(e.Message);
        _logger.LogTrace(e.StackTrace);
        return false;
      }
    }

    public object GetVariable(string variableName)
    {
      try
      {
        var url = PrepareUrlForVariable(variableName);
        _logger.LogInformation(url.ToString());
        var task = _httpClient.GetAsync(url);
        return task.Result.Content;
      }
      catch (Exception e)
      {
        _logger.LogCritical(e.Message);
        _logger.LogTrace(e.StackTrace);
        throw;
      }
    }

    private static Uri PrepareUrlForCommand(string command, params object[] args)
      => new Uri($"{Database.Database.ArduinoIp}/{command}?params={Join("-", args)}");

    private static Uri PrepareUrlForVariable(string variable) => new Uri($"http://{Database.Database.ArduinoIp}/{variable}");
  }
}
