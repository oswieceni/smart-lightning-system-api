﻿using System.Collections.Generic;
using System.Linq;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Database;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.Modules;
using SmartLightningSystemApi.ProfileProperties;

namespace SmartLightningSystemApi.BusinessLogic.Services
{
  public class SettingsService : ISettingsService
  {
    private readonly IEmbeddedConnectionService _embeddedConnectionService;

    public SettingsService(IEmbeddedConnectionService embeddedConnectionService)
    {
      _embeddedConnectionService = embeddedConnectionService;
    }

    public List<Device> GetDevices()
    {
      if(!Database.Database.Devices.Any())
        GetAvailableDevices();
      return Database.Database.Devices.Values.ToList();
    }

    public void UpdateDeviceSettings(int deviceId, ActivityProfileProperties properties)
    {
      AddOrUpdateDeviceSettings(deviceId, Modes.Activity, properties);
    }

    public void UpdateDeviceSettings(int deviceId, FlowProfileProperties properties)
    {
      AddOrUpdateDeviceSettings(deviceId, Modes.Flow, properties);
    }

    public void UpdateDeviceSettings(int deviceId, ManualProfileProperties properties)
    {
      AddOrUpdateDeviceSettings(deviceId, Modes.Manual, properties);
    }

    public void UpdateDeviceSettings(int deviceId, TimeProfileProperties properties)
    {
      AddOrUpdateDeviceSettings(deviceId, Modes.Time, properties);
    }

    public void UpdateDeviceSettings(int deviceId, MotionProfileProperties properties)
    {
      AddOrUpdateDeviceSettings(deviceId, Modes.Motion, properties);
    }

    private static void AddOrUpdateDeviceSettings(int deviceId, Modes mode, ProfileProperties.ProfileProperties properties)
    {
      if (!Database.Database.Devices.ContainsKey(deviceId))
      {
        Database.Database.Devices.Add(deviceId, new Device
        {
          DeviceId = deviceId,
          WorkMode = Modes.Manual,
          Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
          {
            {mode, properties}
          }
        });
        return;
      }

      if (!Database.Database.Devices[deviceId].Settings.ContainsKey(mode))
      {
        Database.Database.Devices[deviceId].Settings.Add(mode, properties);
        return;
      }

      Database.Database.Devices[deviceId].Settings[mode] = properties;
    }

    public Device GetDeviceSettings(int deviceId)
    {
      return Database.Database.Devices[deviceId];
    }

    public void SetWorkMode(int deviceId, Modes workMode)
    {
      if(Database.Database.Devices.ContainsKey(deviceId))
        Database.Database.Devices[deviceId].WorkMode = workMode;
      else
        Database.Database.Devices.Add(deviceId, new Device()
        {
          DeviceId = deviceId,
          WorkMode = workMode,
          Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
        });
    }

    public void ChangeAcuSettings(ArduinoIpSettingsModel arduinoSettings)
    {
      Database.Database.ArduinoIp = arduinoSettings.Ip;
      Database.Database.ArduinoId = arduinoSettings.Id;
      Database.Database.Method = arduinoSettings.Method;
    }

    public ArduinoIpSettingsModel GetAcuSettings()
    {
      return new ArduinoIpSettingsModel
      {
        Method = Database.Database.Method,
        Ip = Database.Database.ArduinoIp,
        Id = Database.Database.ArduinoId
      };
    }

    private void GetAvailableDevices()
    {
      Database.Database.Devices.Add(777780, new Device
      {
        DeviceId = 777780,
        Type = DeviceType.KobiIr,
        DeviceName = "Żarówka 1",
        WorkMode = Modes.Manual,
        Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
      });

      Database.Database.Devices.Add(777776, new Device
      {
        DeviceId = 777776,
        Type = DeviceType.KobiIr,
        DeviceName = "Żarówka 2",
        WorkMode = Modes.Manual,
        Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
      });
      
      Database.Database.Devices.Add(777779, new Device
      {
        DeviceId = 777779,
        Type = DeviceType.LedTape,
        DeviceName = "Pasek LED",
        WorkMode = Modes.Manual,
        Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
      });

//      var devices = _embeddedConnectionService.GetVariable("devices");
//      foreach (var device in (dynamic)devices)
//      {
//        if(!Database.Database.Devices.ContainsKey(device.Id))
//          Database.Database.Devices.Add(device.Id, new Device
//          {
//            DeviceId = device.Id,
//            Type = device.Type,
//            WorkMode = device.WorkMode,
//            DeviceName = device.Name,
//            Settings = new Dictionary<Modes, ProfileProperties.ProfileProperties>()
//          });
//      }
    }
  }
}
