using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;

namespace SmartLightningSystemApi.Services
{
  public class AppSettings : IAppSettings
  {
    public Arduino Arduino { get; }
  }

  public class Arduino
  {
    public string Url { get; set; }
  }
}
