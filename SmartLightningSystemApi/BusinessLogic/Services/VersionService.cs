using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;

namespace SmartLightningSystemApi.BusinessLogic.Services
{
    public class VersionService : IVersionService
    {
        private readonly IAppSettings appSettings;

        public VersionService(IAppSettings appSet)
        {
            appSettings = appSet;
        }

        public string GetApplicationVersion()
        {
            return PlatformServices.Default.Application.ApplicationVersion;
        }

        public string GetApplicationName()
        {
            return PlatformServices.Default.Application.ApplicationName;
        }
    }
}
