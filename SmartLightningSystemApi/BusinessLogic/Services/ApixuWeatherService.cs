﻿using System;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SmartLightningSystemApi.BusinessLogic.Services.Interfaces;
using SmartLightningSystemApi.Database;

namespace SmartLightningSystemApi.BusinessLogic.Services
{
  public class ApixuWeatherService : IWeatherService
  {
    private readonly ILogger<ApixuWeatherService> logger;

    public ApixuWeatherService(ILoggerFactory loggerFactory)
    {
      logger = loggerFactory.CreateLogger<ApixuWeatherService>();
    }

    public Weather GetWeather()
    {
      logger.LogInformation("Calling Apixu API to obtain weather information");
      if (DateTime.UtcNow - Database.Database.Weather.LastUpdated <= TimeSpan.FromHours(1))
        return Database.Database.Weather;
      const string url =
        "https://api.apixu.com/v1/current.json?key=f1dec56660fd4cdeac8154337170205&q=Wroc%C5%82aw&lang=pl";
      var httpClient = new HttpClient();
      var isoDateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm" };
      var result = JsonConvert.DeserializeObject<ApixuWeatherModel>(
        httpClient.GetAsync(url).Result.Content.ReadAsStringAsync().Result, isoDateTimeConverter);
      string icon = result.Current.Condition.Icon;
      var startIndex = icon.LastIndexOf("/") + 1;
      result.Current.Condition.Icon = icon.Substring(startIndex, icon.LastIndexOf(".") - startIndex);
      result.Source = "Apixu";
      Database.Database.Weather = new Weather
      {
        CloudCoverage = result.Current.CloudCoverage,
        Condition = new Database.Condition
        {
          Text = result.Current.Condition.Text,
          Icon = result.Current.Condition.Icon
        },
        IsDay = result.Current.IsDay,
        LastUpdated = result.Current.LastUpdated,
        Location = $"{result.Location.City}, {result.Location.Country}",
        Temperature = result.Current.Temperature,
        TemperatureFeeled = result.Current.TemperatureFeeled,
        Source = result.Source
      };
      return Database.Database.Weather;
    }

    public class ApixuWeatherModel
    {
      [JsonProperty("location")]
      public Location Location { get; set; }

      [JsonProperty("current")]
      public Current Current { get; set; }

      [JsonIgnore]
      public string Source { get; set; }
    }

    public class Location
    {
      [JsonProperty("name")]
      public string City { get; set; }

      [JsonProperty("country")]
      public string Country { get; set; }

      [JsonProperty("lat")]
      public decimal Latitude { get; set; }

      [JsonProperty("lon")]
      public decimal Longtitude { get; set; }

      [JsonProperty("localtime")]
      public string LocalTime { get; set; }
    }

    public class Current
    {
      private DateTime _lastUpdated;

      [JsonProperty("last_updated")]
      public DateTime LastUpdated { get; set; }

      [JsonProperty("temp_c")]
      public decimal Temperature { get; set; }

      [JsonProperty("is_day")]
      public bool IsDay { get; set; }

      [JsonProperty("condition")]
      public Condition Condition { get; set; }

      [JsonProperty("humidity")]
      public byte Humidity { get; set; }

      [JsonProperty("cloud")]
      public byte CloudCoverage { get; set; }

      [JsonProperty("feelslike_c")]
      public decimal TemperatureFeeled { get; set; }

      [JsonProperty("vis_km")]
      public decimal Visibility { get; set; }
    }

    public class Condition
      {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }
      }
  }
}  