﻿namespace SmartLightningSystemApi.BusinessLogic.Services.Interfaces
{
  public interface IEmbeddedConnectionService
  {
    bool SendCommand(string command, params object[] args);
    object GetVariable(string variableName);
  }
}
