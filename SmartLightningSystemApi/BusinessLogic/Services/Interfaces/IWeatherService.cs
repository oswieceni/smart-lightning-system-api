﻿using SmartLightningSystemApi.Database;

namespace SmartLightningSystemApi.BusinessLogic.Services.Interfaces
{
    public interface IWeatherService
    {
      Weather GetWeather();
    }
}