using SmartLightningSystemApi.Services;

namespace SmartLightningSystemApi.BusinessLogic.Services.Interfaces
{
  public interface IAppSettings
  {
    Arduino Arduino { get; }
  }
}
