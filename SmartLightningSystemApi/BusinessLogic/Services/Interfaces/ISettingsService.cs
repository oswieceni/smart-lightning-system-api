﻿using System.Collections.Generic;
using SmartLightningSystemApi.Database;
using SmartLightningSystemApi.Models;
using SmartLightningSystemApi.Modules;
using SmartLightningSystemApi.ProfileProperties;

namespace SmartLightningSystemApi.BusinessLogic.Services.Interfaces
{
  public interface ISettingsService
  {
    List<Device> GetDevices();
    void UpdateDeviceSettings(int deviceId, ActivityProfileProperties properties);
    void UpdateDeviceSettings(int deviceId, FlowProfileProperties properties);
    void UpdateDeviceSettings(int deviceId, ManualProfileProperties properties);
    void UpdateDeviceSettings(int deviceId, TimeProfileProperties properties);
    void UpdateDeviceSettings(int deviceId, MotionProfileProperties properties);
    Device GetDeviceSettings(int deviceId);
    void SetWorkMode(int deviceId, Modes workMode);
    void ChangeAcuSettings(ArduinoIpSettingsModel arduinoSettings);
    ArduinoIpSettingsModel GetAcuSettings();
  }
}
