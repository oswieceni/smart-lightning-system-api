﻿namespace SmartLightningSystemApi.ProfileProperties
{
  public class TimeProfileProperties : ProfileProperties
  {
    public uint StartHour { get; set; }
    public uint StartMinute { get; set; }
    public uint EndHour { get; set; }
    public uint EndMinute { get; set; }
  }
}