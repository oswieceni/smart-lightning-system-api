﻿using SmartLightningSystemApi.Models;

namespace SmartLightningSystemApi.ProfileProperties
{
  public class FlowProfileProperties : ProfileProperties
  {
    public Speed speed { get; set; }
  }
}