﻿using SmartLightningSystemApi.Models;

namespace SmartLightningSystemApi.ProfileProperties
{
  public class ActivityProfileProperties : ProfileProperties
  {
    public Activity Activity { get; set; }
  }
}