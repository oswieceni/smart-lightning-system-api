﻿namespace SmartLightningSystemApi.ProfileProperties
{
  public class MotionProfileProperties : ProfileProperties
  {
    public int Delay { get; set; }
  }
}