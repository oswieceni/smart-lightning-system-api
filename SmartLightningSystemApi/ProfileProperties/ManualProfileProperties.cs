﻿using SmartLightningSystemApi.Models;

namespace SmartLightningSystemApi.ProfileProperties
{
  public class ManualProfileProperties : ProfileProperties
  {
    public State State { get; set; }
    public int Color { get; set; }
    public byte Brightness { get; set; }
  }
}