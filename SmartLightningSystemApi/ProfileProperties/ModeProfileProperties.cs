﻿namespace SmartLightningSystemApi.Models
{
  public class ModeProfileProperties : ProfileProperties.ProfileProperties
  {
    public Modes Mode { get; set; }
  }
}