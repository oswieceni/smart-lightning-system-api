using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Nancy.Owin;
using SmartLightningSystemApi.Services;

namespace SmartLightningSystemApi
{
    public class Startup
    {
        private readonly IConfiguration config;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();

            config = builder.Build();
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            loggerFactory
                .AddConsole(config.GetSection("Logging"))
                .AddDebug();

            var appSettings = new AppSettings();
            config.Bind(appSettings);
            app.UseOwin(x => x.UseNancy(opt => opt.Bootstrapper = new Bootstrapper(appSettings, app.ApplicationServices)));
        }
    }
}
