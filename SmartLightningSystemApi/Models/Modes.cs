﻿namespace SmartLightningSystemApi.Models
{
  public enum Modes
  {
    Manual,
    Motion,
    Time,
    Weather,
    Activity,
    Flow,
    Gesture
  }
}