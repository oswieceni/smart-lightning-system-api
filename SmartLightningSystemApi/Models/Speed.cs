﻿namespace SmartLightningSystemApi.Models
{
  public enum Speed
  {
    Smooth,
    Fade,
    Strobe,
    Flash
  }
}