﻿using System.Collections.Generic;

namespace SmartLightningSystemApi.Models
{
  public class DeviceSettings
  {
    public int DeviceId { get; set; }
    public Modes WorkMode { get; set; }
  }
}